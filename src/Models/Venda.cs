using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Models
{
    public class Venda
    {
        public Venda(int id, string itens)
        {
            this.Id = id;
            this.Itens = itens;
            this.Data = DateTime.Now;
        }
        
        public int Id { get; set; }
        public string Itens { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status{ get; set; }
        public int VendaId { get; set; }
    }
}