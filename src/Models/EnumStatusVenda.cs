namespace src.Models
{
    public enum EnumStatusVenda
    {
        AGUARDANDOPAGAMENTO,
        PAGO,
        TRANSPORTANDO,
        ENTREGUE,
        CAMCELADA
    }
}