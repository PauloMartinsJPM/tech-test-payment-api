using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using src.Context;

namespace src.Models;

[ApiController]
[Route("[controller]")]

public class VendaController : ControllerBase
{
    private DatabaseContext _context { get; set; }
    public VendaController(DatabaseContext context)
    {
        this._context = context;
    }

    [HttpPost("Vendas")]
    public ActionResult<Vendedor> Post([FromBody]Vendedor vendedor)
    {
        try
        {
            _context.Vendedors.Add(vendedor);
            _context.SaveChanges();
        }
        catch(SystemException)
        {
            return BadRequest();
        }
        return Created ("Criado", vendedor);
    }

    [HttpGet("ListagemDeVendas")]
    public ActionResult<List<Venda>> Get()
    {
        var result = _context.Vendedors.Include(v => v.vendas).ToList();
        if(!result.Any()) return NoContent();
        return Ok(result);
    }

    [HttpPut("{Status}")]
    public IActionResult ObterPorStatus(EnumStatusVenda Status)
    {
        return Ok(new {msg = "Status da venda atualizado para " + Status});
    }
}
