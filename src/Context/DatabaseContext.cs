using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using src.Models;

namespace src.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext>options) : base(options)
        {

        }

        public DbSet<Venda>Vendas{ get; set; }
        public DbSet<Vendedor>Vendedors{ get; set; }
        protected override void OnModelCreating (ModelBuilder builder)
        {
            builder.Entity<Vendedor>(tabela => { tabela.HasKey(e => e.Id);
                tabela
                .HasMany(e => e.vendas)
                .WithOne()
                .HasForeignKey(c => c.VendaId);
            });
            builder.Entity<Venda>(tabela =>
            {
                tabela.HasKey(e => e.Id);
            });
        }
    }
}